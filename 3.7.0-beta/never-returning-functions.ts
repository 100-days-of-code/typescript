namespace NeverReturningFunctions {
    let doThingWithString = (x: string) => { return true };
    let doThingWithNumber = (x: number) => { return true };
    
    function dispatch(x: string | number): boolean {
        if (typeof x === "string") {
            return doThingWithString(x);
        }
        else if (typeof x === "number") {
            return doThingWithNumber(x);
        }
        process.exit(1);
    }
}