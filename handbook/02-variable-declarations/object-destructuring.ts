(function() {
    let o = {
        a: "foo",
        b: 12,
        c: "bar"
    };

    let {a, b} = o;
    console.log(a);
    console.log(b);

    ({ a, b } = { a: "baz", b: 101 });

    {
        let { a, ...passthrough } = o;
        let total = passthrough.b + passthrough.c.length;
    }

    {
        let { a: newName1, b: newName2 } = o;
    }

    {
        let { a, b }: { a: string, b: number } = o;
    }

})()