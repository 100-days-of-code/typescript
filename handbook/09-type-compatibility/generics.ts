(() => {
    interface Empty<T> {
    }
    let x: Empty<number>;
    let y: Empty<string>;

    x = y;

    interface NotEmpty<T> {
        data: T;
    }
    let n: NotEmpty<number>;
    let s: NotEmpty<string>;

    // n = s; // error
})();