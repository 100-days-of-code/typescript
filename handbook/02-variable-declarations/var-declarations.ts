(function() {
    function f() {
        var a = 10;
        return function g() {
            var b = a + 1;
            return b;
        }
    }

    var g = f();
    console.log(g());

    function k() {
        var a = 1;

        a = 2;
        var b = l();
        a = 3;
        
        return b;

        function l() {
            return a;
        }
    }

    console.log(k());
})()