let notSure: any = 4;
notSure = "maybe a string instead";
notSure = false;

notSure.ifItExists();
notSure.toFixed();

let prettySure: Object = 4;
// prettySure.toFixed(); -> Error

let anylist: any[] = [1, true, "free"];
anylist[1] = 100;