const numLivesForCat = 9;
const kitty = {
    name: "Aurora",
    numLives: numLivesForCat
}

/* error
kitty = {
    name: "Danielle",
    numLives: numLivesForCat
}
*/

kitty.name = "Rory";
kitty.numLives--;