// Zero based
enum Color { Red, Green, Blue };
let c1: Color = Color.Green;

// One based
enum OneBasedColor { Red = 1, Green, Blue };
let c2: OneBasedColor = OneBasedColor.Green;

// Arbitrary based
enum ArbitraryColor { Red = 1, Green = 2, Blue = 4 };
let c3: ArbitraryColor = ArbitraryColor.Green;

let colorName = Color[1];
console.log(colorName);