(() => {
    let x = [0, 1, "", new Date()];

    // Zoo
    abstract class Animal {
        constructor() {
        }
    }

    class Rhino extends Animal {
        constructor() {
            super();
        }
    }

    class Elephant extends Animal {
        constructor() {
            super();
        }
    }

    class Snake extends Animal {
        constructor() {
            super();
        }
    }

    let zoo = [new Rhino(), new Elephant(), new Snake()];
    let zoo_of_animals: Animal[] = [new Rhino(), new Elephant(), new Snake()];


    function createZoo(): Animal[] {
        return [new Rhino(), new Elephant(), new Snake()];
    }
})();
