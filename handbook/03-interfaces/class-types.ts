interface ClockConstructor {
    new (hour: number, minute: number): ClockInterface;
}

interface ClockInterface {
    currentTime: Date;
    setTime(d: Date): void;
    tick(): void;
}

function createClock(ctor: ClockConstructor, hour: number, minute: number): ClockInterface {
    return new ctor(hour, minute);
}

class Clock implements ClockInterface {
    currentTime: Date = new Date();
    setTime(d: Date) {
        this.currentTime = d;
    }
    tick() {
        throw "what?";
    }
    constructor(h: number, m:number) { }
}

class DigitalClock implements ClockInterface {
    currentTime: Date = new Date();
    setTime(d: Date) {
        this.currentTime = d;
    }
    tick() {
        console.log("beep beep");
    }
    constructor(h: number, m:number) { }
}

class AnalogClock implements ClockInterface {
    currentTime: Date = new Date();
    setTime(d: Date) {
        this.currentTime = d;
    }
    tick() {
        console.log("tick tock");
    }
    constructor(h: number, m:number) { }
}

let digital = createClock(DigitalClock, 12, 17);
let analog = createClock(AnalogClock, 7, 32);