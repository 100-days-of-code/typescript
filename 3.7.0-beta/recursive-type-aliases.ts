namespace RecursiveTypeAliases {
    type ValueOrArray<T> = T | Array<ValueOrArray<T>>;

    type Json =
        | string
        | number
        | boolean
        | null
        | { [property: string]: Json }
        | Json[];
}