let color: string = "blue";
color = 'red';

let fullName: string = 'Bob Bobbington';
let age: number = 37;
let sentence: string = `Hello, my name ist ${ fullName }.

I'll be ${ age + 1 } years old next month.`;