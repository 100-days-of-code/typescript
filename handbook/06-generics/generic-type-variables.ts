(function() {
    function loggingIdentity<T>(arg: T): T {
        // console.log(arg.length);  // Error: T doesn't have .length
        return arg;
    }

    function loggingIdentityArray<T>(arg: T[]): T[] {
        console.log(arg.length);  
        return arg;
    }

    function loggingIdentityArray2<T>(arg: Array<T>): Array<T> {
        console.log(arg.length);  // Array has a .length, so no more error
        return arg;
    }
})()