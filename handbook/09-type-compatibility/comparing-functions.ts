(() => {
    let x = (a: number) => 0;
    let y = (b: number, s: string) => 0;
    y = x;
    // x = y; // error
})();

(() => {
    let items = [1, 2, 3];
    items.forEach((item, index, array) => console.log(item));
    items.forEach(item => console.log(item));
})();

(() => {
    let x = () => ( { name: "Alice" });
    let y = () => ( { name: "Alice", location: "Seattle" });
    x = y;
    // y = x; // error
})();