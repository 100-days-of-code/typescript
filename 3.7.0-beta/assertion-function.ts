namespace AssertionFunctions {
    function assert(condition: any, msg?: string): asserts condition {
        if (!condition) {
            //throw new AssertionError(msg);
        }
    }

    function assertIsString(val: any): asserts val is string {
        if (typeof val !== "string") {
            //throw new AssertionError();
        }
    }

    function yell(str) {
        assert(typeof str === "string");
        assertIsString(str);
        return str.toUpperCase();
    }
}