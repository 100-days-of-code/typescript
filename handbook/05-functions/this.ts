(function() {
    {
        let deck = {
            suits: ["hearts", "spades", "clubs", "diamonds"],
            cards: Array(52),
            createCardPicker: function() {
                return function() {
                    let pickedCard = Math.floor(Math.random() * 52);
                    let pickedSuit = Math.floor(pickedCard / 13);
        
                    return {suit: this.suits[pickedSuit], card: pickedCard % 13};
                }
            }
        }
        
        let cardPicker = deck.createCardPicker();
        let pickedCard = cardPicker();
        
        alert("card: " + pickedCard.card + " of " + pickedCard.suit);
    }

    {
        let deck = {
            suits: ["hearts", "spades", "clubs", "diamonds"],
            cards: Array(52),
            createCardPicker: function() {
                // NOTE: the line below is now an arrow function, allowing us to capture 'this' right here
                return () => {
                    let pickedCard = Math.floor(Math.random() * 52);
                    let pickedSuit = Math.floor(pickedCard / 13);
        
                    return {suit: this.suits[pickedSuit], card: pickedCard % 13};
                }
            }
        }
        
        let cardPicker = deck.createCardPicker();
        let pickedCard = cardPicker();
        
        alert("card: " + pickedCard.card + " of " + pickedCard.suit);
    }

    {
        function f(this: void) {
            // make sure `this` is unusable in this standalone function
        }
    }

    {
        interface Card {
            suit: string;
            card: number;
        }
        interface Deck {
            suits: string[];
            cards: number[];
            createCardPicker(this: Deck): () => Card;
        }
        let deck: Deck = {
            suits: ["hearts", "spades", "clubs", "diamonds"],
            cards: Array(52),
            // NOTE: The function now explicitly specifies that its callee must be of type Deck
            createCardPicker: function(this: Deck) {
                return () => {
                    let pickedCard = Math.floor(Math.random() * 52);
                    let pickedSuit = Math.floor(pickedCard / 13);
        
                    return {suit: this.suits[pickedSuit], card: pickedCard % 13};
                }
            }
        }
        
        let cardPicker = deck.createCardPicker();
        let pickedCard = cardPicker();
        
        alert("card: " + pickedCard.card + " of " + pickedCard.suit);
    }
})()