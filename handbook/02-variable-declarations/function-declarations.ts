(function() {
    type C = { a: string, b?: number }

    {
        function f({ a, b }: C): void {
            
        }
    }

    {
        function g({ a="", b=0 } = {}): void {
            
        }
        g();
    }

    {
        function h({ a, b = 0 } = { a: "" }): void {
            // ...
        }
        h({ a: "yes" }); // ok, default b = 0
        h(); // ok, default to { a: "" }, which then defaults b = 0
        // h({}); // error, 'a' is required if you supply an argument
    }
})()