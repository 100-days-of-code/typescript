(function() {
    let first = [1, 2];
    let second = [3, 4];
    let bothPlus = [0, ...first, ...second, 5];
    console.log(bothPlus);

    let defaults = { food: "spicy", price: "$$", ambiance: "noisy" };

    let search = { food: "rich", ...defaults };
    console.log(search);

    search = { ...defaults, food: "rich" };
    console.log(search);

    class C {
        p = 12;
        m() {
        }
    }
    let c = new C();
    let clone = { ...c };
    console.log(clone.p);
})()