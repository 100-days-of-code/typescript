// public by default
{
    class Animal {
        name: string;
        public constructor(name: string) {
            this.name = name;
        }
        public move(distanceInMeters: number = 0) {
            console.log(`${ this.name } moved ${ distanceInMeters }m.`);        
        }
    }
    new Animal("Cat").name;
}

// private
{
    class Animal {
        private name: string;
        public constructor(name: string) {
            this.name = name;
        }
        public move(distanceInMeters: number = 0) {
            console.log(`${ this.name } moved ${ distanceInMeters }m.`);        
        }
    }
    // new Animal("Cat").name; // error
}

{
    class Animal {
        private name: string;
        constructor(theName: string) { this.name = theName; }
    }
    
    class Rhino extends Animal {
        constructor() { super("Rhino"); }
    }
    
    class Employee {
        private name: string;
        constructor(theName: string) { this.name = theName; }
    }
    
    let animal = new Animal("Goat");
    let rhino = new Rhino();
    let employee = new Employee("Bob");
    
    animal = rhino;
    // animal = employee; // Error: 'Animal' and 'Employee' are not compatible
}

// protected
{
    class Person {
        protected name: string;
        constructor(name: string) { this.name = name; }
    }
    
    class Employee extends Person {
        private department: string;
    
        constructor(name: string, department: string) {
            super(name);
            this.department = department;
        }
    
        public getElevatorPitch() {
            return `Hello, my name is ${this.name} and I work in ${this.department}.`;
        }
    }
    
    let howard = new Employee("Howard", "Sales");
    console.log(howard.getElevatorPitch());
    // console.log(howard.name); // error
}

{
    class Person {
        protected name: string;
        protected constructor(theName: string) { this.name = theName; }
    }
    
    // Employee can extend Person
    class Employee extends Person {
        private department: string;
    
        constructor(name: string, department: string) {
            super(name);
            this.department = department;
        }
    
        public getElevatorPitch() {
            return `Hello, my name is ${this.name} and I work in ${this.department}.`;
        }
    }
    
    let howard = new Employee("Howard", "Sales");
    // let john = new Person("John"); // Error: The 'Person' constructor is protected
}