(function() {
    {
        function buildName(firstName: string, lastName: string) {
            return firstName + " " + lastName;
        }
        
        // let result1 = buildName("Bob");                  // error, too few parameters
        // let result2 = buildName("Bob", "Adams", "Sr.");  // error, too many parameters
        let result3 = buildName("Bob", "Adams");         // ah, just right
    }

    {
        function buildNameOpt(firstName: string, lastName?: string) {
            if (lastName)
                return firstName + " " + lastName;
            else
                return firstName;
        }
        
        let result1 = buildNameOpt("Bob");                  // works correctly now
        // let result2 = buildNameOpt("Bob", "Adams", "Sr.");  // error, too many parameters
        let result3 = buildNameOpt("Bob", "Adams");         // ah, just right
    }

    {
        function buildNameDefault(firstName: string, lastName = "Smith") {
            return firstName + " " + lastName;
        }
        
        let result1 = buildNameDefault("Bob");                  // works correctly now, returns "Bob Smith"
        let result2 = buildNameDefault("Bob", undefined);       // still works, also returns "Bob Smith"
        // let result3 = buildNameDefault("Bob", "Adams", "Sr.");  // error, too many parameters
        let result4 = buildNameDefault("Bob", "Adams");         // ah, just right
    }

    {
        function buildNameFirstDefault(firstName = "Will", lastName: string) {
            return firstName + " " + lastName;
        }
        
        // let result1 = buildNameFirstDefault("Bob");                  // error, too few parameters
        // let result2 = buildNameFirstDefault("Bob", "Adams", "Sr.");  // error, too many parameters
        let result3 = buildNameFirstDefault("Bob", "Adams");         // okay and returns "Bob Adams"
        let result4 = buildNameFirstDefault(undefined, "Adams");     // okay and returns "Will Adams"
    }
})()