(function() {
    interface StringArray {
        [index: number]: string;
    }

    let myArray: StringArray
    myArray = ["Bob", "Fred"];

    let myStr: string = myArray[0];

    class Animal {
        name: string
    }

    class Dog extends Animal {
        breed: string;
    }

    interface NotOkay {
        [x: number]: Animal;
        // [x: string]: Dog;    // error
    }

    interface NumberDictionary {
        [index: string]: number;
        length: number;
        // name: string;    // error
    }

    interface NumberOrStringDictionary {
        [index: string]: number | string;
        length: number;
        name: string;
    }

    interface ReadonlyStringArray {
        readonly [index: number]: string;
    }
    let myArray2: ReadonlyStringArray = ["Alice", "Bob"];
    // myArray2[2] = "Mallory";    // error
})()