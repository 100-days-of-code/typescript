(function() {
    let input = [1, 2];
    let [first, second] = input;
    console.log(first);
    console.log(second);

    [first, second] = [second, first];
    console.log(first);
    console.log(second);

    function f([first, second]: [number, number]) {
        console.log(first);
        console.log(second);
    }
    f([1, 2]);

    {
        let [first, ...rest] = [1, 2, 3, 4];
        console.log(first);
        console.log(rest);
    }

    {
        let [first] = [1, 2, 3, 4];
        console.log(first); // outputs 1
    }

    {
        let [, second, , fourth] = [1, 2, 3, 4];
        console.log(second); // outputs 2
        console.log(fourth); // outputs 4
    }
})()