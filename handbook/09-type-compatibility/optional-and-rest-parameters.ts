(() => {
    function invokeLater(args: any[], callback: (...args: any[]) => void) {
        /* ... Invoke callback with 'args' ... */
    }

    invokeLater([1, 2], (x, y) => console.log(x + ", " + y));
    invokeLater([1, 2], (x?, y?) => console.log(x + ", " + y));
})();