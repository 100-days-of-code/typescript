namespace UncalledFunctionChecks {
    interface User {
        isAdministrator(): boolean;
        notify(): void;
        doNotDisturb?(): boolean;
    }
    
    // later...
    
    // Broken code, do not use!
    function doAdminThing(user: User) {
        // oops!
        if (user.isAdministrator()) {
        }
        else {
            throw new Error("User is not an admin");
        }
    }

    function issueNotification(user: User) {
        if (user.doNotDisturb) {
            // OK, property is optional
        }
        if (user.notify) {
            // OK, called the function
            user.notify();
        }
    }
}