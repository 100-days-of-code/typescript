namespace NullishCoalescing {
    let f: string = null;
    let b = "string";
    let x = f ?? b;
}