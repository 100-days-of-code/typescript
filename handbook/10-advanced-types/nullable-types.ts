namespace NullableTypes {
    let s = "foo";
    s = null; // error, 'null' is not assignable to 'string'
    let sn: string | null = "bar";
    sn = null; // ok

    sn = undefined; // error, 'undefined' is not assignable to 'string | null'

    function f(x: number, y?: number) {
        return x + (y || 0);
    }
    f(1, 2);
    f(1);
    f(1, undefined);
    f(1, null); // error, 'null' is not assignable to 'number | undefined'

    class C {
        a: number;
        b?: number;
    }
    let c = new C();
    c.a = 12;
    c.a = undefined; // error, 'undefined' is not assignable to 'number'
    c.b = 13;
    c.b = undefined; // ok
    c.b = null; // error, 'null' is not assignable to 'number | undefined'

    function f1(sn: string | null): string {
        if (sn == null) {
            return "default";
        }
        else {
            return sn;
        }
    }

    function f2(sn: string | null): string {
        return sn || "default";
    }

    function broken(name: string | null): string {
        function postfix(epithet: string) {
          return name.charAt(0) + '.  the ' + epithet; // error, 'name' is possibly null
        }
        name = name || "Bob";
        return postfix("great");
      }
      
      function fixed(name: string | null): string {
        function postfix(epithet: string) {
          return name!.charAt(0) + '.  the ' + epithet; // ok
        }
        name = name || "Bob";
        return postfix("great");
      }
}