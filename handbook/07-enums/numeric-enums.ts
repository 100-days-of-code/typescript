enum Direction {
    Up = 1,
    Down,
    Left,
    Right
}

enum ResponseMessage {
    No = 0,
    Yes = 1
}

function respond(recipient: string, message: ResponseMessage): void {
    // ...
}

respond("Princess Caroline", ResponseMessage.Yes)