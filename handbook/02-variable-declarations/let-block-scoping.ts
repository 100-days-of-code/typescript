(function() {
    let hello = "hello";

    function f(input: boolean) {
        let a = 300;

        if (input) {
            let b = a + 1;
            return b;
        }

        return /* b */; // error
    }

    try {
        throw "oh no!";
    } catch(e) {
        console.log("Oh well.");
    }

    // console.log(e); // error

    // a++; // error
    let a;

    function foo() {
        return c;
    }

    foo();
    let c;
})()