let someValue: any = "this is a string";
let strLength: number = (<string>someValue).length;

let someOtherValue: any = "this is a string";
let strOtherLength: number = (someOtherValue as string).length;