// Declare a tuple type
let x: [string, number];

// Initialize it
x = ["hello", 10];

console.log(x[0].substring(1));
