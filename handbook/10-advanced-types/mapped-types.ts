namespace MappedTypes {
    interface PersonPartial {
        name?: string;
        age?: number;
    }
    type Readonly<T> = {
        readonly [P in keyof T]: T[P];
    }
    type Partial<T> = {
        [P in keyof T]?: T[P];
    }
    class Person {
        name: string;
    }
    type ReadonlyPerson = Readonly<Person>;
    type PartialPerson = Partial<Person>;

    type PartialWithNewMember<T> = {
        [P in keyof T]?: T[P];
      } & { newMember: boolean }
    type PartialWithNewMemberPerson = PartialWithNewMember<Person>;

    type NullablePerson = { [P in keyof Person]: Person[P] | null }

    type Nullable<T> = { [P in keyof T]: T[P] | null }
    type PartialAlt<T> = { [P in keyof T]?: T[P] }

    
}