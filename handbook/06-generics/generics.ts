(function() {
    function identity(arg: number): number {
        return arg;
    }

    function identityAny(arg: any): any {
        return arg;
    }

    function identityGeneric<T>(arg: T): T {
        return arg;
    }

    let output = identityGeneric<string>("myString");
    output = identityGeneric("myString");
    console.log(output);
})()