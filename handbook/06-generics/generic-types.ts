function identity<T>(arg: T): T {
    return arg;
}
let myIdentity: <T>(arg: T) => T = identity;

function identity2<T>(arg: T): T {
    return arg;
}
let myIdentity2: <U>(arg: U) => U = identity2;

function identity3<T>(arg: T): T {
    return arg;
}
let myIdentity3: {<T>(arg: T): T} = identity3;

interface GenericIdentityFn {
    <T>(arg: T): T;
}
function identityGen<T>(arg: T): T {
    return arg;
}
let myIdentityGen: GenericIdentityFn = identityGen;

interface GenericIdentityFnT<T> {
    (arg: T): T;
}

function identityT<T>(arg: T): T {
    return arg;
}

let myIdentityT: GenericIdentityFnT<number> = identityT;