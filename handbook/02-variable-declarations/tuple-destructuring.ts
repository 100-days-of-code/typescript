(function() {
    let tuple: [number, string, boolean] = [7, "hello", true];
    let [a, b, c] = tuple;
    console.log(a);
    console.log(b);
    console.log(c);

    {
        let [a, ...bc] = tuple;
        console.log(a);
        console.log(bc);    
    }

    {
        let [a, b, c, ...d] = tuple;
        console.log(d);    
    }

    {
        let [a] = tuple;
        let [, b] = tuple;
        console.log(a);
        console.log(b);
        
    }
})()