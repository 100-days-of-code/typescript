(function() {
    function f(x) {
        var x;
        var x;

        if (true) {
            var x;
        }
    }

    let x = 10;
    // let x = 20; // error

    function g(x) {
    //    let x = 100; // error
    }

    function h() {
        let x = 100;
    //    var x = 100; // error
    }

    function k(condition, x) {
        if (condition) {
            let x = 100;
            return x;
        }

        return x;
    }

    console.log(k(false, 0));
    console.log(k(true, 0));

    function sumMatrix(matrix: number[][]) {
        let sum = 0;
        for (let i = 0; i < matrix.length; i++) {
            var currentRow = matrix[i];
            for (let i = 0; i < currentRow.length; i++) {
                sum += currentRow[i];
            }
        }

        return sum;
    }
})()