namespace IndexTypes {
    function pluck<T, K extends keyof T>(o: T, propertyNames: K[]): T[K][] {
        return propertyNames.map(n => o[n]);
    }

    interface Car {
        manufacturer: string;
        model: string;
        year: number;
        ownersAddress: string;
    }
    let taxi: Car = {
        manufacturer: 'Toyota',
        model: 'Camry',
        year: 2014,
        ownersAddress: 'c/o'
    };

    let makeAndModel: string[] = pluck(taxi, ['manufacturer', 'model']);

    let modelYear = pluck(taxi, ['model', 'year']);

    let carProps: keyof Car;

    function getProperty<T, K extends keyof T>(o: T, propertyName: K): T[K] {
        return o[propertyName]; // o[propertyName] is of type T[K]
    }

    let name: string = getProperty(taxi, 'manufacturer');
    let year: number = getProperty(taxi, 'year');

    // error, 'unknown' is not in 'manufacturer' | 'model' | 'year'
    // let unknown = getProperty(taxi, 'unknown');

    interface Dictionary<T> {
        [key: string]: T;
    }
    let keys: keyof Dictionary<number>; // string | number
    let value: Dictionary<number>['foo']; // number

    interface Dictionary<T> {
        [key: number]: T;
    }
    let keys2: keyof Dictionary<number>; // number
    let value2: Dictionary<number>['foo']; // Error, Property 'foo' does not exist on type 'Dictionary<number>'.
    let value3: Dictionary<number>[42]; // number
    
}